from django.urls import include, path
from django.contrib import admin 
from django.conf import settings
from .views import get_data, index, addLike, unLike, get_book, logout



urlpatterns = [
    path('', index, name = 'index'),
    path('get_data/cari=<str:cari>', get_data, name = 'get_data'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('get_book/', get_book, name = 'get_book'),
    path('addLike/', addLike, name = 'addLike'),
    path('unLike/', unLike, name = 'unLike')
]