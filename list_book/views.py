from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def get_data(request, cari="quilting"):
    link = "https://www.googleapis.com/books/v1/volumes?q=" + cari
    print(link)
    data_final = requests.get(link).json()
    return JsonResponse(data_final)

def index(request):
    return render(request, "list_book.html")

    

