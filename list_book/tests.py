from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import ListBookConfig
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab9_url_is_exist(self):
        response = Client().get('/list_book/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_get_data_is_exist(self):
        response = Client().get('/list_book/get_data/cari=quilting')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_index_func(self):
        found = resolve('/list_book/')
        self.assertEqual(found.url_name, 'index')

    def test_lab9_using_get_data_func(self):
        found = resolve('/list_book/get_data/cari=quilting')
        self.assertEqual(found.url_name, 'get_data')

    def test_lab9_home_contain_title(self):
        title = 'Book List'
        response = Client().get('/list_book/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)

    def test_lab9_apps(self):
        self.assertEqual(ListBookConfig.name, 'list_book')
        self.assertEqual(apps.get_app_config('list_book').name, 'list_book')