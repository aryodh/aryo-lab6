from django.urls import include, path
from . import views

app_name = 'story10'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('check_email/', views.check_email, name = 'check_email'),
    path('add_subscriber/', views.add_subscriber, name = 'add_subscriber'),
    path('web_service/', views.web_service, name = 'web_service'),
    path('delete_user/', views.delete_user, name = 'delete_user')
]