$(document).ready(function(){
    ready = true;
    $("#showup").fadeIn(2000);
    $(".loading-page").delay(2000).fadeOut("slow");
    
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }   
        });
    }

    var theme = true;

    $("#change-theme").click(function() {
        $("#body").toggleClass('theme-1', !theme);
        $("#body").toggleClass('theme-2', theme);
        
        
        if(theme) {
            $(".accordion").css({'background-color': '#34495e', 'color': '#bdc3c7'});
            $(".panel").css('background-color', '#353b48');
            $(".notch").text('Light mode');
            $(".notch").css({'background-color': '#fff', 'color': '#34495e'});
            
            
        } else {
            $(".accordion").css({'background-color': '#dcdde1', 'color': '#34495e'});
            $(".panel").css('background-color', '#fff');
            $(".notch").text('Dark mode');
            $(".notch").css({'background-color': '#2c3e50', 'color': '#fff'});
            
            
        }
        theme = !theme;
    })

    
});

var changed = false;
function changeImage() {
    if (!changed) {
        console.log("masukk");
        $("#myImage").fadeOut(0);
        $("#myImage").fadeIn(2000);
        $("#click-me").text("");
        var image = document.getElementById('myImage');
        image.src = '/static/img/aryo.jpg';
    }
    changed = true;
}
 
