from django.shortcuts import render
from .forms import Subscribe_Form
from .models import Subscriber
from django.core.validators import validate_email
from django.http import JsonResponse

# Create your views here.

def index(request):
    response = {
        'subscribe_form' : Subscribe_Form 
    }
    html = 'myProfile.html'
    return render(request, html, response)

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Email format is wrong!',
            'status':'fail'
        })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'Email already exist',
            'status':'fail'
        })
    
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })


def add_subscriber(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            'message':'Subscribe success!'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"There's no GET method here!"
        }, status = 403)


def web_service(request):
    all_data = Subscriber.objects.all().values('name','email')
    data = list(all_data)
    return JsonResponse(data, safe = False)


def delete_user(request):
    print('masuk')
    print(request.POST['email'])
    select_object = Subscriber.objects.filter(email=request.POST['email'],password=request.POST['password'])

    if select_object:
        select_object.delete()
        return JsonResponse({
            'message':request.POST['email'] + ' removed!',
            'deleted':True
        })
    
    return JsonResponse({
        'message':'Incorrect Password',
        'deleted':False
    })


    