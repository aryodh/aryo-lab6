from django import forms

class Subscribe_Form(forms.Form):
    email = forms.CharField(
        label = '',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'email',
            'placeholder':'Email',
            'type':'text',
            'id':'emailForm'
        })
    )

    name = forms.CharField(
        label = '',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'name',
            'placeholder':'Name',
            'type':'text',
            'id':'nameForm'
        })
    )

    password = forms.CharField(
        label = '',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'password',
            'placeholder':'Password',
            'type':'password',
            'id':'passwordForm'
        })
    )