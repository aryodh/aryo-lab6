from django import forms

class Status_Form(forms.Form):
    error_msg = {
        'required' : 'This field is required',
    }

    content_attrs = {
        'type' : 'textarea',
        'class' : 'status-form-textarea',
        'placeholder' : 'Write your status...',
        'rows' : '3px'
    }

    content = forms.CharField(
        label = '',
        required = True,
        max_length = 300,
        widget = forms.Textarea(attrs = content_attrs)
    )
    
