from django.urls import include, path
from . import views

app_name = 'web_tdd'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('add_status', views.add_status, name = 'add_status'),
    path('delete_all_stat_db', views.delete_all_stat_db, name = 'delete_all_stat_db'),
    path('profiles', views.profile, name = 'profile')
]