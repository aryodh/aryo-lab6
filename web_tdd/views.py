from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
# Create your views here.
def index(request):
    response = {}
    response['status_form'] = Status_Form
    html = 'index.html/'
    all_status = Status.objects.all().order_by('-pk')
    response['status'] = all_status

    return render(request, html, response)

def add_status(request):
    response = {}
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['content'] = request.POST['content']
        status = Status(content = response['content'])
        status.save()
    
    return HttpResponseRedirect('/')

def delete_all_stat_db(request):
    Status.objects.all().delete()
    return HttpResponseRedirect('/')
    
def profile(request):
    response = {}
    html = 'profile.html'
    return render(request, html, response)