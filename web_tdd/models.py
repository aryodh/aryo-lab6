from django.db import models
from django.utils import timezone
# Create your models here.
class Status(models.Model):
    content = models.TextField(max_length = 300)
    created_date = models.DateTimeField(default = timezone.now)

