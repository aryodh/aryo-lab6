from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .forms import *
from .models import Status
from .apps import WebTddConfig
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class Lab6UnitTest(TestCase):

    def test_lab6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.url_name, 'index')

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(content = "mau liburan")

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea"', form.as_p())
        self.assertIn('id="id_content"', form.as_p())

    def test_form_validation_for_blank_item(self):  
        form = Status_Form(data={'content':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['content'],
            ['This field is required.']
        )
    
    def test_apps(self):
        self.assertEqual(WebTddConfig.name, 'web_tdd')
        self.assertEqual(apps.get_app_config('web_tdd').name, 'web_tdd')

    def test_lab6_post_success_and_render_the_result(self):
        test = 'mau liburrr'
        response_post = Client().post('/add_status', {'content':test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab6_post_error_and_render_the_result(self):
        test = 'mau liburrr'
        response_post = Client().post('/add_status', {'content':' '})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_lab6_delete_all_stat_db(self):
        response_post = Client().post('/delete_all_stat_db')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 0)

    def test_lab6_profile_url_exist(self):
        response = Client().get('/web_tdd/profiles')
        self.assertEqual(response.status_code, 200)

    def test_lab6_profile_contain_name(self):
        name = 'Aryo Tinulardhi'
        response = Client().get('/web_tdd/profiles')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_lab6_profile_contain_gender(self):
        gender = 'Male'
        response = Client().get('/web_tdd/profiles')
        html_response = response.content.decode('utf8')
        self.assertIn(gender, html_response)

    def test_lab6_profile_contain_npm(self):
        npm = '1706039515'
        response = Client().get('/web_tdd/profiles')
        html_response = response.content.decode('utf8')
        self.assertIn(npm, html_response)

    
class SeleniumTestCase(LiveServerTestCase): 

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(SeleniumTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTestCase, self).tearDown()

    def test_post_on_landing_page(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)  #('http://127.0.0.1:8081/')
        status = "Coba Coba Coba Cobaaa"
        status_box = selenium.find_element_by_name('content')
        time.sleep(1)
        submit_button = selenium.find_element_by_name('submit')

        status_box.send_keys(status)
        submit_button.send_keys(Keys.RETURN)
        time.sleep(3)

        page = selenium.page_source
        self.assertIn(status, page)

    def test_css_on_landing_page(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        submit_button = selenium.find_element_by_class_name('write-content')
        submit_button_color = submit_button.value_of_css_property('background-color')
        self.assertEqual(submit_button_color, 'rgba(245, 248, 250, 1)')

    def test_css_delete_button_color(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)  #('http://127.0.0.1:8081/')
        status = "Coba Coba Coba Cobaaa"
        status_box = selenium.find_element_by_name('content')
        time.sleep(1)
        submit_button = selenium.find_element_by_name('submit')

        status_box.send_keys(status)
        submit_button.send_keys(Keys.RETURN)
        time.sleep(3)

        delete_button = selenium.find_element_by_class_name('delete-button')
        delete_button_color = delete_button.value_of_css_property('background-color')
        self.assertEqual(delete_button_color, 'rgba(231, 76, 60, 1)') 

    def test_layout_hello_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        hello_position = selenium.find_element_by_class_name('write-content')
        self.assertIn('Hello, apa kabar?', hello_position.text)

    def test_write_status_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        write_position = selenium.find_element_by_class_name('status-form-textarea').get_attribute('placeholder')
        self.assertEqual('Write your status...', write_position)

    def test_title_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertEqual('Landing Page', selenium.title)

    def test_js_accordian_one(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/web_tdd/profiles')
        time.sleep(5)
        selenium.find_element_by_xpath('//*[@id="body"]/div[4]/div[3]/button[1]').click()
        self.assertIn("Life", selenium.page_source) 
        time.sleep(10)

    def test_js_accordian_two(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/web_tdd/profiles')
        time.sleep(5)
        selenium.find_element_by_xpath('//*[@id="body"]/div[4]/div[3]/button[2]').click()
        self.assertIn("Life", selenium.page_source) 

    def test_js_theme_changed(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/web_tdd/profiles')
        time.sleep(5)
        body_background = selenium.find_element_by_id('body').value_of_css_property('background-color')
        self.assertIn("rgba(236, 240, 241, 1)", body_background)
        selenium.find_element_by_class_name('notch').click()
        body_background = selenium.find_element_by_id('body').value_of_css_property('background-color')
        self.assertIn("rgba(44, 62, 80, 1)", body_background)


        

