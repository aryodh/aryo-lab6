
$(document).ready(function(){
    $.ajax({
		url: "get_data",
		datatype: 'json',
		success: function(data){
			var result;
			for(var i = 0; i < data.items.length; i++) {
                result += '<tr style="background-size: cover; background-image: linear-gradient(rgba(52, 73, 94,0.9), rgba(to right, 255, 255, 255, 1)), url(' + data.items[i].volumeInfo.imageLinks.smallThumbnail + ');">'
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
				"<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
				"<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
				"<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		document.getElementById("counter").innerHTML = counter;
	}
}



