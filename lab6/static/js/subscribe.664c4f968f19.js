var emailIsValid = false;

$("#emailForm").focusout(function() {
    $("#emailStatus").html("");
    checkEmail();
});

$("#passwordForm").keydown(function() {
    $('#passwordStastus')
});

function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }

    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('submit').prop('disabled', true);
            } else {
                emailIsValid = true;
                checkAll();
            }
            $('#emailForm').append('<small> ${data["message"]} </small>');
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('nameForm').val() !== '' && 
        $('password').val().length > 7) {
        
        $('submit').prop('disabled', false);
    }
}