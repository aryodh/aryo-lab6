var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkEmail();
        checkAll();
    });

    $("#emailForm").keyup(function() {
        checkEmail();
    });
    
    $("#passwordForm").keyup(function() {
        $('#statusForm').html('');
        if ($('#passwordForm').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        } else {
            checkEmail();
        }
        checkAll();
    });

    $('#submit').click(function () {
        data = {
            'email' : $('#emailForm').val(),
            'name' : $('#nameForm').val(),
            'password' : $('#passwordForm').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'add_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('emailForm').value = '';
                document.getElementById('nameForm').value = '';
                document.getElementById('passwordForm').value = '';
                
                $('#statusForm').html('');
                checkAll();
            }
        })
    });

    $.ajax({
        type: 'GET',
        url:'web_service',
        success: function(data) {
            var i = 0
            for(i; i<data.length; i++) {
                content = '<tr class="align-middle">' + 
                "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" + 
                '<td>' + data[i].name + '</td><td>' + data[i].email + 
                '</td><td class="text-center"> <button class="button" value="' + 
                data[i].email + '" onclick="delete_subscriber(this.value)"> Delete Me! </button> </td></tr>';
                $('tbody').append(content)
            }
        }
    });
})



function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
                if ($('#passwordForm').val() !== '' && $('#passwordForm').val().length < 8) {
                    $('#statusForm').html('');
                    $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
                }
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#nameForm').val() !== '' && 
        $('#passwordForm').val() !== '' &&
        $('#passwordForm').val().length > 7) {
        
        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}


function delete_subscriber(email) {
    console.log(email);
    document.getElementsByClassName('modal').style.display = "block";
    
}